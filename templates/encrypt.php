<div class="container" id="encrypt" hidden>

    <div class="form-group">
        <label class="col-md-4 control-label">
            Write something to encrypt
        </label>
        <div class="col-md-4">
                <input id="encrypt_text" type="text" required>
        </div>
    </div>

    <!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <button type="submit" class="btn btn-success" onclick="encrypt()">Encrypt</button>
        </div>
    </div>

    <label class="col-md-4 control-label">
        Encrypted text
    </label>
    <div class="col">
        <textarea id="encrypted_text" rows="4" cols="50"></textarea>
    </div>

    <!-- Button -->
    <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
            <button type="submit" class="btn btn-success" onclick="decrypt()">Decrypt</button>
        </div>
    </div>


    <label class="col-md-4 control-label">
        Decrypted text
    </label>
    <div class="col">
        <textarea id="decrypted_text" rows="4" cols="50" disabled></textarea>
    </div>

</div>