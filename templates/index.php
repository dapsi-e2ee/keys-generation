<?php
script('user_encryption', $script);
script('user_encryption', 'sodium');
script('user_encryption', 'encrypt');
script('user_encryption', 'index');
style("user_encryption", 'style');
?>
<div class="menu" id="actions">
    <a onclick="showForm()" id="title" class="fade"><?php p($title) ?></a>
    <a onclick="showEncryption()" id="test" class="fade">Encrypt/Decrypt</a>
    <a onclick="deleteLocalKey()" id="delete" class="fade">Delete local Key (demo)</a>
    <a onclick="resetServerKey()" id="reset" class="fade">Reset server Key (demo)</a>
</div>

<div class="container" id="keys" hidden>
    <h3><?php p($description) ?></h3>
    <p>ⓘ Provide answer to only one question in each section</p>
    <form class="well form-horizontal" onsubmit="submitForm(event)" id="encryption">
        <fieldset>
            <?php foreach ($questions as $i=>$q) { ?>
                <div class="form-group">
                    <?php p($i+1) ?>.
                    <select name="questions" id="questions">
                    <?php foreach ($q[0] as $j=>$question) { ?>
                        <option value="<?php p($i+1) . '.' . p($j+1) ?>"><?php p($question) ?></option>
                    <?php } ?>
                    </select>
                    <div class="col-md-4">
                        <div class="input-group">
                            <input name="<?php p($i+1) ?>" placeholder="<?php p($q[2]) ?>" class="form-control" type="<?php p($q[1]) ?>">
                        </div>
                    </div>
                </div>
            <?php } ?>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-success"> <?php p($button) ?></button>
                </div>
            </div>

        </fieldset>
    </form>
    <input name="seed" type="hidden" id="serverKey" value="<?php p($seedKey) ?>">
</div>

<?php print_unescaped($this->inc('encrypt')); ?>

<div id="message" class="container">
    
</div>