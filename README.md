# E2E Encryption Key Generation and Recovery

## How does E2E Encryption (Key generation and recovery) app work?
We use libsodium, a popular library used for encryption and decryption. We chose libsodium because it has a support for generating keys in recoverable way, that means even when user loses their private key, he/she can recover it by following the recovery process.

## E2E Encryption basically has two main functions:

- Key Generation
- Key Recovery




## How to setup Keys Generation app in murena cloud?

1. Download source code file in server from https://gitlab.e.foundation/dapsi-e2ee/keys-generation/-/releases/v1.0.0
2. Untar and move it to html/custom_apps, set ownership to www-data
3. Go to apps section in murena cloud and enable E2E Encryption app

This app uses usafe-eval method to access WASM code. In order for this to work, we need to tweak `lib/public/AppFramework/Http/EmptyContentSecurityPolicy.php` at `line 451`, add the following line:

```php
$this->useJsNonce = null;
```

## How is the key generated?

We first ask a series of personal questions to user in various domains, the answer includes text, number, date and phone number. We use these answers to generate a seed-hash based on sha256. Using this seed-hash, we generate a key-pair, called as seed-key-pair. Now we generate the actual user-key-pair using random bytes.

We have two key-pairs now, seed-key-pair and user-key-pair. We generate a third key called server-key which is nothing but a bitwise-OR operation between seed-key-pair's private key and user-key-pair's private key. This key will be stored in server and will be useful for recovery. user-key-pair is stored locally in browser storage.

Now in order to recover user-private-key, we just have to generate same seed-key-pair which is achieved when user answers exactly same answers, and then perform bitwise-OR between server-key and seed-key-pair's private key.
For demo, we provided a means to test encryption and decryption text data. In order to fully test the functionality, we provided a way to delete user private key from local browser storage and delete server-key from server, only for demo purpose.

## Improvements:

- More secured way to store user private key in browser
- Add QR functionality for adding a new session on other device
- Clear storage keys on user log-out and delete action


