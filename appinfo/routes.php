<?php

return [
    'routes' => [
        ['name'=> 'encryption#getUserSeedKey', 'url' => '/encryption/seed_key', 'verb' => 'GET'],
        ['name'=> 'encryption#setUserSeedKey', 'url' => '/encryption/seed_key', 'verb' => 'POST'],
        ['name'=> 'encryption#resetUserSeedKey', 'url' => '/encryption/seed_key', 'verb' => 'DELETE'],

    ]
];

