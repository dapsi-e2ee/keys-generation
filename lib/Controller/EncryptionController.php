<?php

declare(strict_types=1);

namespace OCA\UserEncryption\Controller;

use OCP\IRequest;
use OCP\IUserSession;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\DataResponse;
use OCA\UserEncryption\Service\EncryptionService;

class EncryptionController extends Controller
{
    private $encryptionService;
    private $userSession;

    public function __construct($appName, IRequest $request, IUserSession $userSession, EncryptionService $encryptionService)
    {
        parent::__construct($appName, $request);
        $this->encryptionService = $encryptionService;
        $this->userSession = $userSession;
    }

    /**
     * @NoCSRFRequired
     * @NoAdminRequired
     */
    public function getUserSeedKey()
    {
        $response = new DataResponse();
        $currentUser = $this->userSession->getUser();
        $seedKey = $this->encryptionService->getUserSeedKey($currentUser->getUID());
        $response->setData(['seed_key' => $seedKey]);
        return $response;
    }


    /**
     * @NoCSRFRequired
     * @NoAdminRequired
     */
    public function setUserSeedKey(string $seed)
    {
        $response = new DataResponse();
        if (is_null($seed)) {
            $response->setStatus(400);
            $response->setData(['seed_key' => null]);
            return $response;
        }
        $currentUser = $this->userSession->getUser();
        // // check if seed is already present
        // $seedKey = $this->encryptionService->getUserSeedKey($currentUser->getUID());
        // if (!is_null($seedKey)) {
        //     $response->setStatus(304);
        //     $response->setData(['status' => 'Key already present']);
        //     return $response;
        // }
        $status = $this->encryptionService->setUserSeedKey($currentUser->getUID(), $seed);
        if ($status)
            $response->setStatus(200);
        else
            $response->setStatus(500);
        $response->setData(['status' => $status]);
        return $response;
    }

    /**
     * @NoCSRFRequired
     * @NoAdminRequired
     */
    public function resetUserSeedKey()
    {
        $response = new DataResponse();
        $currentUser = $this->userSession->getUser();
        $status = $this->encryptionService->resetUserSeedKey($currentUser->getUID());
        if ($status)
            $response->setStatus(200);
        else
            $response->setStatus(500);
        $response->setData(['status' => $status]);
        return $response;
    }

}
