<?php

namespace OCA\UserEncryption\AppInfo;

use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\Security\IContentSecurityPolicyManager;

class Application extends App implements IBootstrap {

    public function __construct() {
        parent::__construct('user_encryption');
    }

    public function register(IRegistrationContext $context): void {
        // ... registration logic goes here ...
        
        
    }

    public function boot(IBootContext $context): void {
        // ... boot logic goes here ...

        $container = $context->getServerContainer();
        /** @var IContentSecurityPolicyManager */
		$manager = $container->get(IContentSecurityPolicyManager::class);

		$policy = new \OCP\AppFramework\Http\EmptyContentSecurityPolicy();

		$policy->addAllowedScriptDomain('\'self\'');
        $policy->addAllowedScriptDomain('\'unsafe-inline\'');
        $policy->addAllowedScriptDomain('\'unsafe-eval\'');
		

		$policy->allowInlineScript(true);
		$policy->allowEvalScript(true);
		$policy->addAllowedConnectDomain('data:');

		$policy->addAllowedConnectDomain('\'unsafe-inline\'');
		$policy->useJsNonce(null);
		
		$manager->addDefaultPolicy($policy);
    }

}