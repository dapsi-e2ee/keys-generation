<?php

declare(strict_types=1);

namespace OCA\UserEncryption\Service;

use OCP\IConfig;
use UnexpectedValueException;

class EncryptionService
{

    /** @var array */
    private $appName;

    /** @var IConfig */
    private $config;

    public function __construct($appName, IConfig $config)
    {
        $this->appName = $appName;
        $this->config = $config;
    }

    public function setUserSeedKey(string $uid, string $seedKey): bool
    {
        try {
            $this->config->setUserValue($uid, $this->appName, 'user-seedkey', $seedKey);
            return true;
        } catch (UnexpectedValueException $e) {
            return false;
        }
    }

    public function resetUserSeedKey(string $uid): bool
    {
        try {
            $this->config-> deleteUserValue($uid, $this->appName, 'user-seedkey');
            return true;
        } catch (UnexpectedValueException $e) {
            return false;
        }
    }

    public function getUserSeedKey(string $uid)
    {
        try {
            $seedKey = $this->config->getUserValue($uid, $this->appName, 'user-seedkey', null);
            return $seedKey;
        } catch (UnexpectedValueException $e) {
            return null;
        }
    }
}
