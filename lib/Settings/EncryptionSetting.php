<?php

declare(strict_types=1);

namespace OCA\UserEncryption\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\IUserSession;
use OCP\Settings\ISettings;
use OCA\UserEncryption\Service\EncryptionService;

class EncryptionSetting implements ISettings
{

	
	private $l10nFactory;
	private $userSession;

	public function __construct(
		EncryptionService $encryptionService,
		IUserSession $userSession,
	) {
		$this->userSession = $userSession;
		$this->encryptionService = $encryptionService;
	}

	public function getForm(): TemplateResponse
	{
		
		$uid =   $this->userSession->getUser()->getUID();
		//$this->encryptionService->resetSeedKey($uid);
		$seedKey = $this->encryptionService->getUserSeedKey($uid);		
		$title  = 'Encryption Signup';
		$script = 'generate';
		$button = 'Generate';
		$description = 'Please answer the following questions to generate a unique key for you';
		if (!is_null($seedKey)) {
			$title  = 'Create/Recover Keys';
			$script = 'recover';
			$button = 'Recover';			
			$description = 'In order to recover your private key, you need to answer the following question (case sensitive)';
		}
		$parameters = [
			'questions' => [
				[["What was your first pet name?", "What is your favourite color?", "What is you best friend's name?"], "text", "Tommy"],
				[["What is your mother's date of birth?", "when did you get married?", "what is your date of birth?"], "date", ""],
				[["What is your favourite pin number?", "What is your social security number?", "What is your postal pin code?"],  "number", "1234"],
				[["What is your favourite word?", "What is your favorite sport?", "what is your favorite artist's name?"], "text", "Basketball"],
				[["What was your first phone number?"], "tel", "9090909090"]
			],
			'title' => $title,
			'script' => $script,
			'button' => $button,
			'seedKey' => $seedKey,
			'description' => $description
		];
		return new TemplateResponse('user_encryption', 'index', $parameters, '');
	}

	public function getSection(): ?string
	{
		return 'user_encryption';
	}

	public function getPriority(): int
	{
		return 0;
	}
}
