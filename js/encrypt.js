

function encrypt() {
    const text = document.getElementById("encrypt_text").value

    // get keys from storage
    let userPrivKey = localStorage.getItem('userPrivKey')
    if (userPrivKey == null) {
        window.alert("No keys found in local storage")
        return
    }
    userPrivKey = sodium.from_hex(userPrivKey)
    const nonce = "a nonce used for demo 12"
    const cipher = sodium.crypto_secretbox_easy(text, nonce, userPrivKey)
    document.getElementById('encrypted_text').value = sodium.to_hex(cipher)

}


function decrypt() {
    let cipher = document.getElementById("encrypted_text").value
    if (cipher === "") {
        return
    }
    cipher = sodium.from_hex(cipher)
    let userPrivKey = localStorage.getItem('userPrivKey')
    if (userPrivKey == null) {
        window.alert("No keys found in local storage")
        return
    }
    let dec = new TextDecoder();
    userPrivKey = sodium.from_hex(userPrivKey)
    const nonce = "a nonce used for demo 12"
    try {
        const decipher = sodium.crypto_secretbox_open_easy(cipher, nonce, userPrivKey)
        document.getElementById('decrypted_text').value = dec.decode(decipher)
    } catch {
        window.alert("Invalid private key for this encrypted message")
        return
    }
    
    

}

window.addEventListener('load', (event) => {
    const key = localStorage.getItem('userPrivKey');
    if(key)
        showEncryption()        
    else
        showForm()
  });