function showForm(){
    document.getElementById('message').hidden = true
    document.getElementById('keys').hidden = false
    document.getElementById('encrypt').hidden = true
    document.getElementById('test').className = 'fade';
    document.getElementById('title').classList.remove('fade')
    document.getElementById('title').classList.add('primary')
}

function showEncryption(){
    document.getElementById('message').hidden = true
    document.getElementById('encrypt').hidden = false
    document.getElementById('keys').hidden = true
    document.getElementById('title').className = 'fade';
    document.getElementById('test').classList.remove('fade')
    document.getElementById('test').classList.add('primary')
}

function deleteLocalKey(){
    const key = localStorage.getItem('userPrivKey');
    if(key == null) {
        document.getElementById('message').innerHTML = "No keys found to delete"
        document.getElementById('message').hidden = false
    } else {
      localStorage.removeItem("userPrivKey");
      document.getElementById('message').innerHTML = "Local Key deleted"
      document.getElementById('message').hidden = false
    }
}

function resetServerKey(){
    fetch("/apps/user_encryption/encryption/seed_key", {
        method: "DELETE",
        headers: {'Content-Type': 'application/json'}        
      }).then(res => {
        document.getElementById('message').innerHTML = "Server Key deleted";
        document.getElementById('message').hidden = false;
        location.reload();
      });   
}