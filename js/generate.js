async function submitForm(event) {
    event.preventDefault();
    const data = Object.fromEntries(new FormData(event.target).entries());
    const seedValues = Object.values(data)
    if (seedValues.includes("")) {
      document.getElementById('message').innerHTML = "Please answer all questions"
      document.getElementById('message').hidden = false
      return
    }
    const seed = seedValues.join("")
    const seedHash = sodium.crypto_hash_sha256(sodium.from_string(seed))
    const seedKeypair = sodium.crypto_box_seed_keypair(seedHash)
    const seedPrivKey = seedKeypair.privateKey

    //console.log(sodium.to_hex(seed_keypair.privateKey))
    const randomBytes = sodium.randombytes_buf(32, sodium.crypto_SECRETBOX_KEYBYTES)
    const userKeyPair = sodium.crypto_box_seed_keypair(randomBytes)
    const userPrivKey = userKeyPair.privateKey

    const serverKey = Uint8Array.from(seedPrivKey, (v, i) => v ^ userPrivKey[i]) // perform XOR
    // store userPrivKey in browser storage
    localStorage.setItem('userPrivKey', sodium.to_hex(userPrivKey));
    localStorage.setItem('seedPrivKey', sodium.to_hex(seedPrivKey));
    console.log(sodium.to_hex(serverKey))
    console.log(sodium.to_hex(userPrivKey))
    console.log(sodium.to_hex(seedPrivKey))
    const x = {"seed": sodium.to_hex(serverKey)}
    // Make API call to store serverKey in server
    fetch("/apps/user_encryption/encryption/seed_key", {
        method: "POST",
        headers: {'Content-Type': 'application/json'}, 
        body: JSON.stringify(x)
      }).then(res => {
        console.log("Request complete! response:", res);
        location.reload();
      });
}

