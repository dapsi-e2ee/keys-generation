async function submitForm(event) {
    event.preventDefault();
    const data = Object.fromEntries(new FormData(event.target).entries());
    const seedValues = Object.values(data)
    if (seedValues.includes("")) {
        document.getElementById('message').innerHTML = "Please answer all questions"
        document.getElementById('message').hidden = false
        return
    }
    const seed = seedValues.join("")
    const seedHash = sodium.crypto_hash_sha256(sodium.from_string(seed))
    const seedKeypair = sodium.crypto_box_seed_keypair(seedHash)
    const seedPrivKey = seedKeypair.privateKey
    let serverKey = document.getElementById('serverKey').value
    serverKey = sodium.from_hex(serverKey)
    const userPrivKey = Uint8Array.from(seedPrivKey, (v, i) => v ^ serverKey[i]) // perform XOR

    // store userPrivKey in browser storage
    localStorage.setItem('userPrivKey', sodium.to_hex(userPrivKey));
    localStorage.setItem('seedPrivKey', sodium.to_hex(seedPrivKey));
    location.reload();
}